import argparse

import sys
import errno
import os
from os.path import isfile, join

import rasterio
from rasterio.enums import Resampling
from affine import Affine


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test resampling raster file")
    parser.add_argument("-i", "--image", dest="image", help="Input image to resample", required=True)
    parser.add_argument("-s", "--scale", dest="scale", help="Scale factor", required=True)
    parser.add_argument("-o", "--output", dest="output", help="Output resampled image", required=True)

    args = parser.parse_args()

    image_path = args.image
    try:
        if not isfile(image_path):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), image_path)

        # Open file and get original resolution
        dataset = rasterio.open(image_path)
        affine = dataset.transform
        print(f"Image original resolution is {affine.a} m.")

        # Resample data
        scale = int(args.scale)
        out_shape = (dataset.count, dataset.height * scale, dataset.width * scale)
        data = dataset.read(out_shape=out_shape, resampling=Resampling.bilinear)

        # Create new affine transformation
        out_affine = dataset.transform * Affine.scale(
            (dataset.width / data.shape[-1]),
            (dataset.height / data.shape[-2])
        )

        # Write them in output file
        with rasterio.open(args.output, 'w', height=out_shape[1], width=out_shape[2], driver="GTiff", count=1,
                           dtype=data.dtype, crs=dataset.crs, transform=out_affine) as dst:
            dst.write(data)
            dst.close()

        # Close file
        dataset.close()

    except Exception as e:
        print(str(e), file=sys.stderr)
