#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np

from rasterio import features
from affine import Affine
from shapely.geometry import box
from tqdm import tqdm

if __name__ == "__main__":

    minx = 678973.0
    maxx = 679048.0
    miny = 5117334.0
    maxy = 5117445.0

    test_geom = box(minx, miny, maxx, maxy)
    im_mask = np.zeros((20, 20))
    geo_transform = (678900.0, 10.0, 0.0, 5117500.0, 0.0, -10.0)
    affine = Affine.from_gdal(*geo_transform)
    print(affine)

    col, row = 10, 10
    center_x, center_y = affine * (col, row)
    print(f"center : {center_x}, {center_y}")
    for i in tqdm(range(0, 100000)):
        test_mask = [(test_geom, 1), ]
        features.rasterize(test_mask, out_shape=im_mask.shape, transform=affine, out=im_mask)
