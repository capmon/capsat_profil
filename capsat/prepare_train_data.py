import os
import errno
import sys, traceback

from tqdm import tqdm

import argparse
import yaml

import numpy as np
import pandas as pd

from sqlalchemy import create_engine, text, MetaData, Table, Column, Float, String
from sqlalchemy.event import listen

from capsat.dbutils import load_spatialite, load_stat_aggregates


def prepare_df(engine, id, code):
    """
    Prepares useful features to train AI.
    :param engine:
    :param id:
    :param code:
    :return:
    """
    sql = text("""
        SELECT date, ndvi, bsi, coh 
        FROM join_profile 
        WHERE lpis_id = :id
    """)
    df = pd.read_sql(sql, engine, params={'id': id}, parse_dates=['date'], index_col='date')

    df = df.resample('1D').mean()
    df['coh'] = df['coh'].interpolate().round(4)
    df['d_coh'] = np.abs(df['coh'].diff()).round(4)

    win = df['coh'].rolling(36, min_periods=2, center=True)
    df['coh_min'] = win.min()
    df['coh_max'] = win.max()
    df['coh_delta'] = win.max() - win.min()

    df.drop(df.loc[df['ndvi'].isna()].index, inplace=True)

    df['month'] = df.index.month
    df['code'] = code

    df.dropna(inplace=True)

    df['random'] = np.random.uniform(0, 1, df.shape[0])
    df['dataset'] = 'train'
    df.loc[df['random'] >= 0.85, 'dataset'] = 'test'
    df.loc[df['random'] <= 0.15, 'dataset'] = 'cv'

    return df


if "__main__" == __name__:

    parser = argparse.ArgumentParser(
        description="Prepare and store data which can be used to train AI to recognize coverage from coherence")

    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    args = parser.parse_args()

    try:
        # Load parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']

        # Connect database
        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)
        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, "connect", load_spatialite)
        listen(engine, "connect", load_stat_aggregates)

        # Create train table
        with engine.connect() as conn:
            metadata = MetaData(bind=conn)
            train = Table(
                'train_data',
                metadata,
                Column('code', String),
                Column('ndvi', Float),
                Column('bsi', Float),
                Column('coh', Float),
                Column('d_coh', Float),
                Column('coh_min', Float),
                Column('coh_max', Float),
                Column('coh_delta', Float),
                Column('month', Float),
                Column('dataset', String)
            )
            train.drop(checkfirst=True)
            train.create()

            # Get train data and insert them to table
            sql = text(f"""
                SELECT {lpis_id} AS lpis_id, code_cultu AS code
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu
                ORDER BY random()
            """)

            sql_total = text(f"""
                SELECT count(*) AS count
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu
            """)
            total = conn.execute(sql_total).fetchone()['count']

            proxy = conn.execution_options(stream_results=True).execute(sql)

            with tqdm(total=total) as pbar:
                while True:
                    batch = proxy.fetchmany(100)
                    if not batch:
                        break
                    to_insert = list()
                    for row in batch:
                        id = row['lpis_id']
                        code = row['code']
                        try:
                            pdf = prepare_df(engine, id, code)
                            to_insert.extend(pdf.to_dict('records'))
                        except Exception:
                            continue
                        pbar.update()
                    conn.execute(train.insert(), to_insert)

            conn.execute("CREATE INDEX idx_train_data_code ON train_data(code)")

    except Exception as e:
        traceback.print_exc(file=sys.stderr)