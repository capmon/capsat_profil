import os
import errno
import sys, traceback

from tqdm import tqdm

import argparse
import yaml

import pandas as pd

from sqlalchemy import create_engine, text, MetaData, Table, Column, Date, Float, String
from sqlalchemy.event import listen

from capsat.dbutils import load_spatialite, load_stat_aggregates


def create_indexes(engine, lpis_table, lpis_id):
    """
    Creates some useful indexes on LPIS database.

    :param engine:
    :param lpis_table:
    :param lpis_id:
    :return:
    """
    with engine.connect() as conn:
        conn.execute(text(f"CREATE INDEX IF NOT EXISTS idx_{lpis_table}_{lpis_id} ON {lpis_table}({lpis_id})"))
        conn.execute(text(f"CREATE INDEX IF NOT EXISTS idx_{lpis_table}_code_cultu ON {lpis_table}(code_cultu)"))
        conn.execute(text(f"CREATE INDEX IF NOT EXISTS idx_s2_profile_lpis_id ON s2_profile(lpis_id)"))
        conn.execute(text(f"CREATE INDEX IF NOT EXISTS idx_s1_coh_profile_lpis_id ON s1_coh_profile(lpis_id)"))



if "__main__" == __name__:

    parser = argparse.ArgumentParser(description="Merge S1 and S2 profile tables")

    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    args = parser.parse_args()

    try:

        # Load parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']
        crop_codes_path = params['parameters']['crop_codes_path']

        # Crop codes file exists ?
        if not os.path.exists(crop_codes_path):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), crop_codes_path)

        # Connect database
        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)
        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, "connect", load_spatialite)
        listen(engine, "connect", load_stat_aggregates)

        # Create useful indexes
        create_indexes(engine, lpis_table, lpis_id)

        # Create crop_code table
        with engine.connect() as conn:
            date_columns = ['sowing_start', 'sowing_end', 'growing_start', 'growing_end', 'harvest_start', 'harvest_end']
            crop_codes = pd.read_csv(crop_codes_path, index_col='code', parse_dates=date_columns)
            for col in date_columns:
                crop_codes[col] = crop_codes[col].dt.date
            crop_codes.to_sql("crop_code", conn, if_exists='replace')

            # Create empty join_profile table
            metadata = MetaData(bind=conn)
            join_profile = Table(
                'join_profile',
                metadata,
                Column('date', Date),
                Column('lpis_id', String),
                Column('code', String),
                Column('ndvi', Float),
                Column('ndvi_median', Float),
                Column('ndvi_var', Float),
                Column('bsi', Float),
                Column('bsi_median', Float),
                Column('bsi_var', Float),
                Column('masked_per', Float),
                Column('coh', Float),
                Column('coh_median', Float),
                Column('coh_var', Float)
            )
            join_profile.drop(checkfirst=True)
            join_profile.create()

            # Loop through LPIS table
            sql = text(f"""
                SELECT l.{lpis_id} AS id, l.code_cultu
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu 
            """)
            sql_total = text(f"""
                SELECT count(*) AS count
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu
            """)
            total = conn.execute(sql_total).fetchone()['count']

            sql_ndvi = text(f"""
                SELECT date, mean AS ndvi, median AS ndvi_median, var AS ndvi_var, masked_per
                FROM s2_profile
                WHERE type = 'NDVI'
                AND lpis_id = :id
            """)
            sql_bsi = text(f"""
                SELECT date, mean AS bsi, median AS bsi_median, var AS bsi_var
                FROM s2_profile
                WHERE type = 'BSI'
                AND lpis_id = :id
            """)
            sql_coh = text("""
                SELECT end_date AS date, mean AS coh, median AS coh_median, var AS coh_var
                FROM s1_coh_profile
                WHERE lpis_id = :id
            """)

            proxy = conn.execution_options(stream_result=True).execute(sql)
            with tqdm(total=total) as pbar:
                while True:
                    batch = proxy.fetchmany(100)
                    if not batch:
                        break
                    to_insert = list()
                    for row in batch:
                        id = row['id']
                        code = row['code_cultu']

                        # Get NDVI, BSI and Coherence profile separately
                        ndvi = pd.read_sql(sql_ndvi, conn, params={'id': id}, parse_dates=['date'], index_col='date')
                        bsi = pd.read_sql(sql_bsi, conn, params={'id': id}, parse_dates=['date'], index_col='date')
                        coh = pd.read_sql(sql_coh, conn, params={'id': id}, parse_dates=['date'], index_col='date')

                        # Merge them into one dataframe
                        df = ndvi.join(bsi).join(coh, how='outer')
                        # This makes date as a "normal" column, which will be later considered as a value to insert.
                        df.reset_index(inplace=True)

                        # Insert them to table (using directly sql alchemy is faster than panda's to_sql method
                        values = df.to_dict('records')
                        values = [dict({'lpis_id': id, 'code': code}, **value) for value in values]
                        to_insert.extend(values)
                        pbar.update()

                    conn.execute(join_profile.insert(), to_insert)

            conn.execute("CREATE INDEX idx_join_profile_lpis_id ON join_profile(lpis_id)")
            conn.execute("CREATE INDEX idx_join_profile_code ON join_profile(code)")

    except Exception as e:
        traceback.print_exc(file=sys.stderr)


