import os
import errno
import sys, traceback

from tqdm import tqdm

import argparse
import yaml
import logging.config

from datetime import datetime

from sqlalchemy import create_engine, text, MetaData, Table, Column, Integer, String
from sqlalchemy.event import listen

import numpy as np
import pandas as pd

from joblib import load as load_pipe

import multiprocessing as mp

from capsat.dbutils import load_spatialite, load_stat_aggregates
from capsat.log import init_log_process, format_duration
from capsat.profile import get_parcel_profile


_ai_columns = ['coh', 'd_coh', 'coh_min', 'coh_max', 'coh_delta', 'autumn', 'winter', 'spring', 'summer']


_worker_logger = None
_params = None
_engine = None
_models = None
_anomaly_detector = None


def init_log_by_worker(queue: mp.Queue):
    """
    Init logging inside a process (worker) to a QueueHandler.
    Log records will be registered to queue.

    :param queue: Queue to register records
    :return:
    """
    queue_handler = logging.handlers.QueueHandler(queue)

    current_worker_name = mp.current_process().name

    # _worker_logger is global for each process (each process has its own).
    global _worker_logger
    _worker_logger = logging.getLogger(current_worker_name)

    # All existing handlers are removed in order to avoid logging to root logger.
    for h in _worker_logger.handlers:
        _worker_logger.removeHandler(h)
    _worker_logger.addHandler(queue_handler)
    _worker_logger.setLevel(logging.DEBUG)


def load_ai_models(codes, models_path):
    """
    Loads AI models for crop codes.

    :param codes: List of crop codes
    :param models_path: Path to the directory containing models (in joblib format)
    :return: List of models
    """
    models = dict()
    for code in codes:
        pipe_path = os.path.join(models_path, f"{code}_pipe.joblib")
        if not os.path.exists(pipe_path):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), pipe_path)
        models[code] = load_pipe(pipe_path)
    return models


def init_worker(params, queue):
    """
    Init global variables and logger for one worker.

    :param params: Project parameters
    :param queue: Log queue
    :return:
    """
    # Get parameters
    global _params
    _params = params
    lpis_database = params['parameters']['lpis_database']
    models_path = params['parameters']['models_path']

    # Connect database
    global _engine
    if not os.path.exists(lpis_database):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)
    _engine = create_engine(f"sqlite:///{lpis_database}")
    listen(_engine, "connect", load_spatialite)
    listen(_engine, "connect", load_stat_aggregates)

    # Get all AI models
    global _models
    sql_codes = text(f"SELECT code FROM crop_code")
    with _engine.connect() as conn:
        codes = [row['code'] for row in conn.execute(sql_codes)]
        _models = load_ai_models(codes, models_path)

    # Get anomaly detector
    global _anomaly_detector
    anomaly_detector_path = os.path.join(models_path, 'anomaly_detector.joblib')
    if not os.path.exists(anomaly_detector_path):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), anomaly_detector_path)
    _anomaly_detector = load_pipe(anomaly_detector_path)

    # Logging inside worker
    init_log_by_worker(queue)


def do_worker_unpack(i, row):
    """
    Analyses one parcel. Unpacked version of do_worker.

    :param i: Loop index. Not used.
    :param row: SQL query row containing one parcel.
    :return: Coverage information for one parcel.
    """

    ndvi_cover_threshold = _params['parameters']['ndvi_cover_threshold']
    ndvi_bare_threshold = _params['parameters']['ndvi_bare_threshold']

    lpis_id = row['lpis_id']
    code = row['code']
    try:
        df = get_parcel_profile(_engine, lpis_id, _anomaly_detector)
    except Exception as e:
        _worker_logger.error(f"Error when getting data for lpis_id {lpis_id}")
        _worker_logger.exception(e)
        return None

    # Does parcel have NDVI values ? (no NDVI is very rare, but it can happen on tile boundaries)
    has_ndvi = ('ndvi' in df)

    # Initiate coverage indications
    df['cover_ndvi'] = False
    df['cover_coh'] = False

    # Predict coverture from coherence
    df_coh = df[_ai_columns].dropna()
    X = df_coh.values
    y = _models[code].predict(X)
    df.loc[df_coh.index, 'cover_coh'] = (y == 1)

    if has_ndvi:
        # Remove abnormal values from coherence prediction
        df.loc[(df.ndvi < ndvi_bare_threshold + 0.1) & df.trusted_ndvi, 'cover_coh'] = False

        # Predict coverture from NDVI
        df['cover_ndvi'] = (
            ((df.ndvi >= ndvi_cover_threshold - 0.1) & (df.d_ndvi > 0))  # Growth part
            | (df.ndvi >= ndvi_cover_threshold)  # Everything beyond threshold
        ) & df.trusted_ndvi

    # Finally, combine all coverage indications
    df['cover'] = (df.cover_ndvi | df.cover_coh)

    return lpis_id, code, df


def do_worker(args):
    """
    Callback for one worker. Analyses one parcel.

    :param args: Packed args
    :return:
    """
    return do_worker_unpack(*args)


def analyse_profile(log_queue):
    """
    Analyses all profiles using a multiprocessing.Pool

    :param log_queue: Queue to record log
    :return:
    """
    lpis_table = _params['parameters']['lpis_table']
    lpis_id = _params['parameters']['lpis_id']

    try:
        with _engine.connect() as conn:
            # Create table containing results
            metadata = MetaData(bind=conn)
            coverage = Table(
                'coverage',
                metadata,
                Column('lpis_id', String, primary_key=True),
                Column('code', String),
                Column('covered_tr1', Integer),
                Column('covered_tr2', Integer),
                Column('covered_tr3', Integer),
                Column('covered_tr4', Integer),
                Column('bare_tr1', Integer),
                Column('bare_tr2', Integer),
                Column('bare_tr3', Integer),
                Column('bare_tr4', Integer)
            )
            coverage.drop(checkfirst=True)
            coverage.create()

            # Experimentation dates
            experiment_start = pd.to_datetime(_params['parameters']['experiment_start'], format="%Y-%m-%d").date()
            experiment_end = pd.to_datetime(_params['parameters']['experiment_end'], format="%Y-%m-%d").date()

            # Store cumulative coverage information in a dataframe
            codes = [row['code'] for row in conn.execute("SELECT code FROM crop_code")]
            count_codes = dict.fromkeys(codes, 0)  # Store number of processed parcel by code
            cumulative_df = pd.DataFrame(index=pd.date_range(experiment_start, experiment_end), columns=codes, dtype=np.float32)
            cumulative_df[:] = 0  # Store cumulative coverage information

            # Count total number of parcels to analyse
            sql_total = text(f"""
                SELECT count(*) AS count
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu
            """)
            total = conn.execute(sql_total).fetchone()['count']

            # Create pool with 3 workers
            p = mp.Pool(16, initializer=init_worker, initargs=(_params, log_queue,))

            # Scanning all LPIS parcels
            sql_select = text(f"""
                SELECT l.{lpis_id} AS lpis_id, l.code_cultu AS code
                FROM {lpis_table} l
                JOIN crop_code c ON c.code = l.code_cultu
            """)
            proxy = conn.execution_options(stream_results=True).execute(sql_select)

            with tqdm(total=total) as pbar:
                while True:
                    batch = proxy.fetchmany(1000)
                    if not batch:
                        break
                    to_insert = list()

                    # Execute worker for each feature in batch
                    for pack in p.imap_unordered(do_worker, enumerate(batch), chunksize=100):
                        if pack is None:
                            continue

                        lpis_id, code, df = pack

                        # Reduce result to experimentation date range
                        df_experiment = df[experiment_start:experiment_end]

                        # Cumulate coverage by day
                        count_codes[code] += 1
                        cumulative_df.loc[df_experiment.index, code] += df_experiment['cover']

                        # Count covered and bare soil by quarter
                        mask_tr1 = np.isin(df_experiment.index.month, [11, 12, 1])
                        mask_tr2 = np.isin(df_experiment.index.month, [2, 3, 4])
                        mask_tr3 = np.isin(df_experiment.index.month, [5, 6, 7])
                        mask_tr4 = np.isin(df_experiment.index.month, [8, 9, 10])
                        covered_tr1 = df_experiment.loc[mask_tr1 & df_experiment.cover].shape[0]
                        covered_tr2 = df_experiment.loc[mask_tr2 & df_experiment.cover].shape[0]
                        covered_tr3 = df_experiment.loc[mask_tr3 & df_experiment.cover].shape[0]
                        covered_tr4 = df_experiment.loc[mask_tr4 & df_experiment.cover].shape[0]
                        bare_tr1 = df_experiment.loc[mask_tr1 & ~df_experiment.cover].shape[0]
                        bare_tr2 = df_experiment.loc[mask_tr2 & ~df_experiment.cover].shape[0]
                        bare_tr3 = df_experiment.loc[mask_tr3 & ~df_experiment.cover].shape[0]
                        bare_tr4 = df_experiment.loc[mask_tr4 & ~df_experiment.cover].shape[0]

                        to_insert.append({
                            'lpis_id': lpis_id,
                            'code': code,
                            'covered_tr1': covered_tr1,
                            'covered_tr2': covered_tr2,
                            'covered_tr3': covered_tr3,
                            'covered_tr4': covered_tr4,
                            'bare_tr1': bare_tr1,
                            'bare_tr2': bare_tr2,
                            'bare_tr3': bare_tr3,
                            'bare_tr4': bare_tr4
                        })

                        _worker_logger.debug(f"Id {lpis_id} analysed.")
                        pbar.update()

                    # Insert batch of results
                    conn.execute(coverage.insert(), to_insert)

            p.close()
            p.join()

            # Persist cumulative coverage
            for code in codes:
                if count_codes[code] != 0:
                    cumulative_df[code] = cumulative_df[code] / count_codes[code]
            cumulative_df.to_sql('coverage_probability', conn, if_exists='replace', index_label='date', method='multi')

    except KeyboardInterrupt:
        print("Exit")

    except Exception as e:
        _worker_logger.exception(e)
        p.terminate()
        p.join()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Temporal profile analysis")

    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    # Log level
    parser.add_argument("--log", dest="log_conf", help="Log file configuration",
                        required=False, default="etc/log.yml")

    args = parser.parse_args()

    try:
        # Load parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)

        # Logging process
        if not os.path.exists(args.log_conf):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.log_conf)
        log_queue = mp.Queue(-1)
        log_process = mp.Process(target=init_log_process, args=(log_queue, args.log_conf))
        log_process.start()

        # Init global variables for main process (also initializes logging in main process)
        init_worker(params, log_queue)

        message = f"Start main process with PID {mp.current_process().pid}"
        _worker_logger.info(message)
        start = datetime.now()

        analyse_profile(log_queue)

    except Exception as e:
        _worker_logger.exception(e)
        traceback.print_exc(file=sys.stderr)

    finally:
        end = datetime.now()
        _worker_logger.info(f"End (duration: {format_duration(start, end)}")

        # Stop logging
        log_queue.put(None)
        log_process.join()
