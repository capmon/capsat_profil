import os
import errno
import sys,traceback

import logging
import logging.config
import multiprocessing as mp

from datetime import datetime
from math import floor

import yaml


def get_duration(start: datetime, end: datetime):
    """
    Get duration time between start and end.

    :param start: Start datetime
    :param end: End datetime
    :return: Tuple (days, hours, minutes, seconds)
    """
    d = end - start
    total_s = floor(d.total_seconds())
    total_m = total_s // 60
    total_h = total_m // 60
    total_d = total_h // 24
    s = total_s % 60
    m = total_m % 60
    h = total_h % 24
    d = floor(total_d)
    return d, h, m, s


def format_duration(start: datetime, end: datetime):
    """
    Format duration to string.

    :param start: Start datetime
    :param end: End datetime
    :return: Formated string
    """
    duration = get_duration(start, end)
    return f"{duration[0]} day(s), {duration[1]} hour(s), {duration[2]} minute(s), {duration[3]} second(s)"


def init_log_process(queue: mp.Queue, configuration_file):
    """
    Get log records from internal loggers (in queue) and send them to app logger.

    :param queue: Queue where logs are recorded (shared by all workers)
    :param configuration_file: Logging configuration file
    :return:
    """
    if not os.path.exists(configuration_file):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), configuration_file)
    with open(configuration_file, 'r') as f:
        dict_config = yaml.safe_load(f)
        logging.config.dictConfig(dict_config)
    logger = logging.getLogger('app')

    while True:
        try:
            record = queue.get()
            if record is None:
                # Poison pill
                break
            if record.levelno >= logger.level:
                logger.handle(record)
        except Exception:
            traceback.print_exc(file=sys.stderr)

