import os
import errno

from queue import Queue

import numpy as np

import rasterio
from rasterio.enums import Resampling

import fiona

from shapely.geometry import shape, box

from .tile import Tile, S1CoherenceTile, S2Tile, S2Band, window_shape


class DataProvider(object):
    """
    Class to manipulate large amount of rasters in a directory.
    """

    def __init__(self, data_root_dir):
        """
        Initialize a data provider.

        :param data_root_dir: Raster directory
        :param filter_func: Function to apply to filter raster (must be filter_func(t: Tile))
        """
        self._root_dir = os.path.abspath(data_root_dir)
        self._tiles = list()

        self._open_ds = dict()
        self._max_open_ds = 500
        self._open_ds_queue = Queue()

        self._current = -1

    def _fill_tiles(self):
        """
        Fill self.tiles with tiles in self.root_dir

        :return:
        """
        pass

    def size(self):
        """
        Get number of files in this provider.

        :return:
        """
        return len(self._tiles)

    def _get_raster_ds(self, raster_path):
        """
        Get raster dataset.

        Dataset is managed by a Queue, so only self.max_open_ds are opened in the same time.
        :param raster_path: Path to raster file
        :return: rasterio dataset
        """
        if raster_path not in self._open_ds:
            if len(self._open_ds) < self._max_open_ds:
                raster_ds = rasterio.open(raster_path)
                self._open_ds[raster_path] = raster_ds
                self._open_ds_queue.put(raster_path)
            else:
                # Queue is full, so we close & remove opened dataset.
                remove_path = self._open_ds_queue.get()
                remove_ds = self._open_ds.pop(remove_path)
                remove_ds.close()
                raster_ds = rasterio.open(raster_path)
                self._open_ds[raster_path] = raster_ds
                self._open_ds_queue.put(raster_path)

        return self._open_ds[raster_path]

    def close(self):
        """
        Closes all dataset in self.open_ds.
        :return:
        """
        for path, raster_ds in self._open_ds.items():
            raster_ds.close()

        self._open_ds = dict()
        with self._open_ds_queue.mutex:
            self._open_ds_queue.queue.clear()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __iter__(self):
        return DataProviderIterator(self)


class DataProviderIterator(object):

    def __init__(self, provider: DataProvider):
        self._provider = provider
        self._current = -1

    def __next__(self):
        self._current += 1
        if self._current >= self._provider.size():
            raise StopIteration
        return self._provider._tiles[self._current]


class S1DataProvider(DataProvider):
    """
    Class to manipulate Sentinel 1 coherence rasters.
    """

    def __init__(self, data_root_dir):
        super().__init__(data_root_dir)
        self._fill_tiles()

    def _fill_tiles(self):
        """
        Fill self.tiles with tiles in self.root_dir

        :return:
        """
        for f in os.listdir(self._root_dir):
            path = os.path.join(self._root_dir, f)
            tile = S1CoherenceTile(path)
            self._tiles.append(tile)
        # Sort by start_date
        self._tiles.sort(key=lambda x: x.start_date)

    def coherence(self, tile: S1CoherenceTile, bounds):
        """
        Read coherence values in tile inside bounds.

        :param tile:
        :param bounds:
        :return: Numpy array with coherence values
        """
        window, _ = tile.get_window_affine_10m(bounds)
        raster_ds = self._get_raster_ds(tile.path)
        coh = raster_ds.read(1, window=window, out_dtype=np.float)
        return coh


class S2DataProvider(DataProvider):
    """
    Class to manipulate large amount of Sentinel 2 rasters in THEIA format.
    """

    # Suffixes for each band.
    band_dict = {
        "2": "_FRE_B2",
        "3": "_FRE_B3",
        "4": "_FRE_B4",
        "5": "_FRE_B5",
        "6": "_FRE_B6",
        "7": "_FRE_B7",
        "8": "_FRE_B8",
        "8A": "_FRE_B8A",
        "11": "_FRE_B11",
        "12": "_FRE_B12"
    }

    best_resolution = 10

    def __init__(self, data_root_dir):
        """

        :param data_root_dir:
        """
        super().__init__(data_root_dir)
        self.subdir_list = [os.path.join(self._root_dir, o) for o in os.listdir(self._root_dir)
                            if os.path.isdir(os.path.join(self._root_dir, o))]
        self._fill_tiles()

    def _fill_tiles(self):
        """
        Fill dict of tiles in self.root_dir
        """
        for s2_dir in self.subdir_list:
            # first test if directory is a THEIA SENTINEL2 archive. It should begin by SENTINEL2A_ ou SENTINEL2B_
            tile_name = os.path.split(s2_dir)[1]
            if tile_name[0:11] not in ["SENTINEL2A_", "SENTINEL2B_"]:
                continue
            tile_path = os.path.abspath(s2_dir)
            tile = S2Tile(tile_path)
            self._tiles.append(tile)

        # Sort self.tiles by date
        self._tiles.sort(key=lambda x: x.date)

    def mask(self, tile: S2Tile, bounds):
        """
        Combines cloud, edge & saturation masks into one for bounds.

        :param tile: Tile
        :param bounds: Geographical bounds
        :return: array of mask data
        """
        cloud_mask, edge_mask, saturation_mask = tile.get_masks()
        mask_list = []
        window_10m, _ = tile.get_window_affine_10m(bounds)
        for mask in (cloud_mask, edge_mask, saturation_mask):
            mask_ds = self._get_raster_ds(mask)
            mask_array = mask_ds.read(1, window=window_10m, out_dtype=np.uint8)
            mask_list.append(mask_array)
        # Transform from uint8 to boolean
        cloud_array = (mask_list[0] > 0)
        edge_aray = (mask_list[1] > 0)
        saturation_array = (mask_list[2] > 0)
        # Combine all masks into one.
        mask_all = np.logical_or.reduce([cloud_array, edge_aray, saturation_array])
        return mask_all

    def _read_band(self, band: S2Band, window, out_dtype=np.int16, out_shape=None):
        """
        Read data from band and inside window, and resample it if resolution is different from best_resolution.
        Returns array data, with shape out_shape

        :param band: Band
        :param window: Window (in pixels) where to read data
        :param: out_dtype: DType of returned data (default is np.int16, as Sentinel 2 data are)
        :param out_shape: shape of returned data (in case of resampling)
        :return: Tuple of numpy arrays : (data, resampling mask)
        """
        band_ds = self._get_raster_ds(band.path)
        resolution = band.resolution
        resampling_kwargs = dict()
        if resolution != S2DataProvider.best_resolution and out_shape is not None:
            resampling_kwargs = {
                "out_shape": out_shape,
                "resampling": Resampling.bilinear
            }
        data = band_ds.read(1, window=window, out_dtype=np.int16, **resampling_kwargs)
        # Sentinel 2 data are coded with int16, but to avoid int16 limitations, it could be interesting to convert them
        # to something else.
        if out_dtype != np.int16:
            data = data.astype(out_dtype)
        # Find pixels < 0 (they appear when resampling is near masked pixels, and should not be used in any calculation)
        resampling_mask = data < 0
        return data, resampling_mask

    def ndvi(self, tile: S2Tile, bounds):
        """
        Compute NDVI index for one tile and given bounds.

        :param tile: S2Tile
        :param bounds:
        :return: NDVI index as a numpy array
        """
        data = dict()
        resampling_masks = list()
        for b in ['4', '8']:
            band = tile.bands[b]
            window, _ = band.get_window_affine(bounds)
            data[b], rmask = self._read_band(band, window, np.int32)
            resampling_masks.append(rmask)
        div84 = (data['8'] + data['4']) * 1.0
        div84[div84 <= 0] = 0.001
        num84 = (data['8'] - data['4']) * 1.0
        ndvi = np.divide(num84, div84)
        resampling_mask = np.logical_or.reduce(resampling_masks)
        return ndvi, resampling_mask

    def bsi(self, tile, bounds):
        """
        Compute BSI index for one tile and given bounds.

        :param tile: S2Tile
        :param bounds:
        :return: BSI index as a numpy array
        """
        window_10m, affine_10m = tile.get_window_affine_10m(bounds)
        shape_10m = window_shape(window_10m)
        data = dict()
        resampling_masks = list()
        for b in ['2', '4', '8', '11']:
            band = tile.bands[b]
            window, _ = band.get_window_affine(bounds)
            data[b], rmask = self._read_band(band, window, np.int32, shape_10m)
            resampling_masks.append(rmask)
        sum114 = (data['11'] + data['4']) * 1.0
        sum82 = (data['8'] + data['2']) * 1.0
        num = sum114 - sum82
        den = sum114 + sum82
        den[den <= 0] = 0.001
        bsi = np.divide(num, den)
        resampling_mask = np.logical_or.reduce(resampling_masks)
        return bsi, resampling_mask
