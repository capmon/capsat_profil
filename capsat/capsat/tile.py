import os
from os import listdir
from os.path import isfile, join

import math
from datetime import datetime

import rasterio
from rasterio import features

from affine import Affine

import numpy as np

from shapely.geometry import shape, box
from shapely.ops import cascaded_union


def rowcol(x, y, affine, op=math.floor):
    """
    Get row/col for a x/y
    :param x: X coordinate (East)
    :param y: Y coordinate (North)
    :param affine: Affine transformation (r,c) -> (x,y) (from image to geographic coordinates)
    :param op: Math operation to apply (default = math.floor)
    :return: row, col tuple
    """
    r = int(op((y - affine.f) / affine.e))
    c = int(op((x - affine.c) / affine.a))
    return r, c


def bounds_to_window(bounds, affine):
    """
    Create a full cover rasterio-style window from geometric bounds
    :param bounds: Tuple (West, South, East, North)
    :param affine: Affine transformation between image and geographic coordinates.
    :return: Tuple of tuple ((row_start, row_end), (col_start, col_end))
    """
    w, s, e, n = bounds
    row_start, col_start = rowcol(w, n, affine)
    row_stop, col_stop = rowcol(e, s, affine, op=math.ceil)
    return (row_start, row_stop), (col_start, col_stop)


def window_to_bounds(window, affine):
    """
    Get geographical bounds from rasterio-style window
    :param window: Tuple of tuple ((row_start, row_end), (col_start, col_end))
    :param affine: Affine transformation between image and geographic coordinates
    :return: Tuple (West, South, East, North)
    """
    (row_start, row_stop), (col_start, col_stop) = window
    w, s = (col_start, row_stop) * affine
    e, n = (col_stop, row_start) * affine
    return w, s, e, n


def window_affine_from_bounds(affine, bounds):
    """
    Get window and affine transformation from bounds.
    Pixels are added if width or height is only 1 pixel.
    Affine is redefined to match this window / bounds.

    :param affine: Input affine
    :param bounds:
    :return: Tuple (window, affine)
    """
    # Add pixels to window if necessary
    window = bounds_to_window(bounds, affine)
    if window[0][0] == window[0][1]:  # row_start == row_end
        window = (window[0][0], window[0][0] + 1), window[1]
    if window[1][0] == window[1][1]:  # col_start == col_end
        window = window[0], (window[1][0], window[1][0] + 1)
    c, _, _, f = window_to_bounds(window, affine)  # c ~ west, f ~ north
    a, b, _, d, e, _, _, _, _ = tuple(affine)
    out_affine = Affine(a, b, c, d, e, f)

    return window, out_affine


def window_shape(window):
    """
    Get shape (height, width) of a window

    :param window:
    :return: Tuple (height, width)
    """
    return (window[0][1] - window[0][0]), (window[1][1] - window[1][0])


class Band:

    path = None
    resolution = None

    def __init__(self, path):
        # To be completed with common S1 & S2 properties
        pass


class S2Band(Band):

    resolutions = {
        "1": 60,
        "2": 10,
        "3": 10,
        "4": 10,
        "5": 20,
        "6": 20,
        "7": 20,
        "8": 10,
        "8A": 20,
        "9": 60,
        "10": 60,
        "11": 20,
        "12": 20
    }

    def __init__(self, path, affine: Affine = None):
        self.path = path
        self.number = os.path.splitext(os.path.basename(path))[0].split("_")[-1][1:]
        self.resolution = S2Band.resolutions[self.number]
        self.affine = None
        if affine is not None and affine.a != self.resolution:
            scale_factor = self.resolution / affine.a
            self.affine = affine * Affine.scale(scale_factor)
        else:
            self.affine = affine

    def get_window_affine(self, bounds):
        """
        Get window and affine transformation from bounds.

        :param bounds:
        :return: Tuple (window, affine)
        """
        return window_affine_from_bounds(self.affine, bounds)


class Tile:

    NODATA = None

    def __init__(self, path):
        self.path = path
        self.name = os.path.splitext(os.path.basename(path))[0]

    def _get_raster_metadata(self, raster=None):
        """
        Get tile CRS and bounds (from red band)

        :param raster: If given metadata will be read from this band, else from self.path
        :return: Tuple (crs, affine, bounds)
        """
        if raster is None:
            raster = self.path
        ds = rasterio.open(raster)
        crs = ds.crs
        affine = ds.transform
        bounds = ds.bounds
        ds.close()
        return crs, affine, bounds

    def get_window_affine_10m(self, bounds):
        """
        Get window and affine matching bounds.

        :param bounds:
        :return: Tuple (window, affine)
        """
        pass

    def get_nodata_geometry(self, tolerance=10):
        """
        Get shapely geometry for area in tile where there is no data (ie: value 0.)

        :param tolerance: Tolerance for geometry simplification (default is 10m)
        :return: Shapely geometry
        """
        with rasterio.open(self.path) as ds:
            data = ds.read(1)
            nodata = np.logical_or(data == self.NODATA, np.isnan(data))
            # Vectorize nodata
            nodata_shapes = features.shapes(data, mask=nodata, transform=ds.transform)
            polygons = [shape(geom).simplify(tolerance).buffer(0) for geom, v in nodata_shapes]
            # Geometry is simplified, otherwise there would be one vertex per pixel.
            union = cascaded_union([p.simplify(tolerance) for p in polygons])
            return union

    def get_data_geometry(self, tolerance=10):
        """
        Get shapely geometry of not null pixels.

        :param tolerance: Tolerance for geometry simplification
        :return: Shapely geometry
        """
        nodata = self.get_nodata_geometry(tolerance)
        with rasterio.open(self.path) as ds:
            bounds = box(*ds.bounds)
            return bounds.difference(nodata)


class S1CoherenceTile(Tile):
    """
    Represents the result of coherence computation (done with software SNAP)
    """

    NODATA = 0

    def __init__(self, path):
        super().__init__(path)
        # We assume that the file has this format: Coh_S1[A|B]_STARTDATE_ENDDATE_ORBIT_[ASC|DESC]_[POLARIZATION].tif
        # ex: Coh_S1A_20190104T173933_20190110T173902_59_ASC_VV.tif
        tokens = self.name.split("_")
        self.satellite = tokens[1]
        self.start_date = datetime.strptime(tokens[2], "%Y%m%dT%H%M%S")
        self.end_date = datetime.strptime(tokens[3], "%Y%m%dT%H%M%S")
        self.polarization = tokens[-1].upper()

        # Quick fix for 12 days coherence (no orbit nor ascending in filename)
        self.orbit = None
        self.ascending = None
        if len(tokens) > 5:
            self.orbit = int(tokens[4])
            self.ascending = True if tokens[5] == 'ASC' else False

        self.crs, self.affine_10m, self.bounds = super()._get_raster_metadata()

    def get_window_affine_10m(self, bounds):
        """
        Get window and affine matching bounds.

        :param bounds:
        :return: Tuple (window, affine)
        """
        return window_affine_from_bounds(self.affine_10m, bounds)


class S2Tile(Tile):

    NODATA = -10000

    band_dict = {
        "2": "_FRE_B2",
        "3": "_FRE_B3",
        "4": "_FRE_B4",
        "5": "_FRE_B5",
        "6": "_FRE_B6",
        "7": "_FRE_B7",
        "8": "_FRE_B8",
        "8A": "_FRE_B8A",
        "11": "_FRE_B11",
        "12": "_FRE_B12"
    }

    def __init__(self, path):
        super().__init__(path)
        # tile name should follow theia name convention as  SENTINEL2A_20171125-110358-455_L2A_T30UXV_D_V1-4
        # first we extract date 20171125
        self.date_str = self.name.split("_")[1].split("-")[0]
        self.date = datetime.strptime(self.date_str, '%Y%m%d')
        self.mgrs_id = self.name.split("_")[3]
        # Affine is retrieved here, from red band (10 m) because we can't afford to read each band file
        # (performance issue). This affine will be corrected for each band by a scale factor.
        red_band = os.path.join(self.path, self.name + "_FRE_B4.tif")
        self.crs, self.affine_10m, self.bounds = super()._get_raster_metadata(red_band)
        self.bands = self._get_bands()

    def _get_bands(self):
        """
        Get bands for this tile.

        :param affine: Affine transformation
        :return: Dict of Band
        """
        files = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        fre_list = [f for f in files if "_FRE_" in f and f.endswith(".tif")]
        if not fre_list:
            raise ValueError(f"No *_FRE_*.tif found in tile folder {self.path}.")
        bands = dict()
        for f in fre_list:
            full_path = os.path.join(self.path, f)
            band = S2Band(full_path, self.affine_10m)
            bands[band.number] = band
        return bands

    def get_masks(self):
        """
        Get paths to masks files (only 10 m resolution).

        :return: Tuple (cloud, edge, saturation)
        """
        prefix = os.path.join(self.path, "MASKS")
        cloud_mask = os.path.join(prefix, self.name + "_CLM_R1.tif")
        edge_mask = os.path.join(prefix, self.name + "_EDG_R1.tif")
        saturation_mask = os.path.join(prefix, self.name + "_SAT_R1.tif")
        return cloud_mask, edge_mask, saturation_mask

    def get_window_affine_10m(self, bounds):
        """
        Get window and affine transformation for bounds, based on a band with a resolution of 10m.
        In case of resampling data for lower resolution, it can be used as a replacement for original (window, affine)
        :param bounds:
        :return: (window, affine)
        """
        # Red band has a resolution of 10 m
        red_band = self.bands["4"]
        window, affine = red_band.get_window_affine(bounds)
        return window, affine




