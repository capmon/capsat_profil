from datetime import datetime

import numpy as np
import numpy.ma as ma

import utm
from pyproj import Proj, Transformer

from rasterio import features

from shapely.geometry import shape
from shapely.ops import transform as shape_transform

from sqlalchemy import text

import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin

from .provider import DataProvider, S2DataProvider, S1DataProvider


class TemporalProfileBuilder(object):

    def __init__(self, provider: DataProvider, srid=2154):
        self._provider = provider
        self._crs = f"epsg:{srid}"
        self._proj = Proj(self._crs)
        self._wgs84 = Proj("epsg:4326")

    def _get_utm_epsg_code(self, point, in_proj=None):
        """
        Get EPSG code in WGS84 / UTM from given point

        :param point: Input point
        :param in_proj: Input projection, if None, it will be self.in_proj
        :return: EPSG code
        """
        # Project point to WGS84 lon/lat.
        if in_proj is None:
            in_proj = self._proj
        transformer = Transformer.from_proj(in_proj, self._wgs84, always_xy=True)
        pt_long, pt_lat = transformer.transform(point.x, point.y)
        # Retrieve UTM zone
        utm_coord = utm.from_latlon(pt_lat, pt_long)
        utm_zone = utm_coord[2]
        if pt_lat >= 0:
            epsg_code = f'epsg:326{utm_zone:02}'
        else:
            epsg_code = f'epsg:327{utm_zone:02}'
        return epsg_code

    def _project_to_utm(self, geom, in_proj):
        """
        Project geom to UTM coordinates.

        :param geom: Shapely geometry
        :param in_proj: Geometry projection
        :return: Tuple (West, South, East, North)
        """
        # Get representative point (centroid ?)
        pt_in_geom = geom.representative_point()
        # Get EPSG code in WGS84 / UTM system
        epsg_code = self._get_utm_epsg_code(pt_in_geom, self._proj)
        # Reproject geom to correct utm zone
        project = Transformer.from_crs(self._crs, epsg_code, always_xy=True).transform
        utm_geom = shape_transform(project, geom)
        return utm_geom

    def get_temporal_profile(self, geom, feature):
        """
        Get temporal profile for a given geometry

        :param geom: Input geometry (LPIS parcel)
        :param feature: Type of index (NDVI or BSI or coherence)
        :return:
        """
        pass

    def _get_geometry_mask(self, utm_geom, shape, affine):
        """
        Get shape mask raster.
        True means pixel is masked, ie: outside of utm_geom.
        False means inside of utm_geom.

        :param utm_geom: Geometry to rasterize and from which the mask is computed.
        :param shape: Shape of the result numpy array.
        :param affine: Affine transformation
        :return: Mask data as a numpy array (with True/False pixel)
        """
        gmask = [(utm_geom, 1), ]
        im_gmask = features.rasterize(gmask, out_shape=shape, transform=affine)
        # Inverse mask. True = masked = outside geometry
        im_gmaskb = (im_gmask < 1)
        return im_gmaskb


class S1TemporalProfileBuilder(TemporalProfileBuilder):

    def __init__(self, s1_provider: S1DataProvider, srid=2154):
        super().__init__(s1_provider, srid)

    def get_temporal_profile(self, geom, feature, orbit=None, polarization=None):
        """
        Compute temporal profile for a given geometry.

        :param geom: Input shapely geometry
        :param feature: Feature to compute (only coherence for S1)
        :param orbit: Orbit of S1 raster to read
        :param polarization: Polarization of S1 raster to read
        :return:
        """
        utm_geom = self._project_to_utm(geom, self._proj)

        if orbit and polarization:
            gen = (t for t in self._provider if (t.orbit == orbit and t.polarization == polarization))
        else:
            gen = (t for t in self._provider)

        profile = list()
        for tile in gen:
            _, affine = tile.get_window_affine_10m(utm_geom.bounds)
            coherence = self._provider.coherence(tile, utm_geom.bounds)
            if np.all(coherence == 0):
                continue
            gmask = self._get_geometry_mask(utm_geom, coherence.shape, affine)
            gcoherence = ma.masked_array(coherence, mask=gmask)
            mean = round(float(gcoherence.mean()), 2)
            var = round(float(gcoherence.var()), 2)
            median = round(float(ma.median(gcoherence)), 2)
            stat = {
                "type": "coherence",
                "start_date": tile.start_date,
                "end_date": tile.end_date,
                "orbit": tile.orbit,
                "ascending": tile.ascending,
                "polarization": tile.polarization,
                "mean": mean,
                "var": var,
                "median": median
            }
            profile.append(stat)

        return profile


class S2TemporalProfileBuilder(TemporalProfileBuilder):

    def __init__(self, s2_provider: S2DataProvider, srid=2154):
        """

        :param s2_provider:
        """
        super().__init__(s2_provider, srid)

    def get_temporal_profile(self, geom, feature):
        """
        Get temporal profile for a given geometry

        :param geom: Input geometry (LPIS parcel)
        :param feature: Type of index (NDVI or BSI)
        """

        # Get geom in utm coordinates
        utm_geom = self._project_to_utm(geom, self._proj)

        profile = list()

        for tile in self._provider:

            # Compute index
            if "NDVI" == feature:
                index, rmask = self._provider.ndvi(tile, utm_geom.bounds)
            if "BSI" == feature:
                index, rmask = self._provider.bsi(tile, utm_geom.bounds)

            # Get utm_geom mask
            _, affine = tile.get_window_affine_10m(utm_geom.bounds)
            smask = self._provider.mask(tile, utm_geom.bounds)
            im_gmaskb = self._get_geometry_mask(utm_geom, smask.shape, affine)

            # Count pixels inside and outside geometry
            nb_pix_mask = im_gmaskb.sum()
            nb_pix_geom = smask.shape[0] * smask.shape[1] - nb_pix_mask

            # Combine geometry mask and other masks (clouds, etc).
            mask = np.logical_or.reduce([im_gmaskb, smask, rmask])

            if mask.all():
                # If everything is masked, we do not have data
                stat = {
                    "type": feature,
                    "date": tile.date,
                    "mean": None,
                    "median": None,
                    "var": None,
                    "masked_per": round(100, 2)
                }
            else:
                # Get index data where there is no mask, and compute mean, median & variance values.
                masked_index = ma.masked_array(index, mask=mask)

                index_mean = round(float(masked_index.mean()), 2)
                index_var = round(float(masked_index.var()), 2)
                index_median = round(float(ma.median(masked_index)), 2)
                masked_pourcent = round(float((mask.sum() - nb_pix_mask) / nb_pix_geom), 2)
                stat = {
                    "type": feature,
                    "date": tile.date,
                    "mean": index_mean,
                    "median": index_median,
                    "var": index_var,
                    "masked_per": int(100 * masked_pourcent)}

            profile.append(stat)

        return profile


class BSILogTransformer(BaseEstimator, TransformerMixin):

    def __init__(self):
        super().__init__()

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None, **fit_params):
        X_copy = X.copy()
        # This makes BSI distribution more symetric, even if it is not normally distributed
        X_copy[:, 0] = np.log(-X[:, 0] + 1.1)  # BSI is in [-1, 1]. +1.1 makes certain we don't compute negative log.
        return X_copy


def get_parcel_profile(conn, id, anomaly_detector=None):
    """
    Get prepared Dataframe with parcel NDVI and coherence information (resampled with a period of 1D and
    addition of information for prediction with AI).

    :param conn: SQLAlchemy connection or engine
    :param id: LPIS parcel id
    :param anomaly_detector: Trained anomaly detector AI model
    :return: Dataframe with all needed information for guessing coverage
    """
    # Get NDVI and coherence from database
    sql = text(f"""
        SELECT date, ndvi, bsi, coh
        FROM join_profile
        WHERE lpis_id = :id
    """)
    df = pd.read_sql(sql, conn, params={'id': id}, parse_dates=['date'], index_col='date')

    has_ndvi = df['ndvi'].any()

    # Resample NDVI by 1D after removing NaN values and interpolate intermediate dates
    # (begin and end of period must not be interpolated)
    if has_ndvi:
        ndvi = df['ndvi'].dropna().resample('1D').mean().interpolate()
        bsi = df['bsi'].dropna().resample('1D').mean().interpolate()

    # Resample all dataframe
    df = df.resample('1D').mean()

    # Get indication where NDVI is "real", ie not interpolated
    df['real_ndvi'] = False
    df['trusted_ndvi'] = False
    df['d_ndvi'] = np.NaN
    if has_ndvi:
        df.loc[df.ndvi.notna(), 'real_ndvi'] = True

        # Reset NDVI with interpolated NDVI
        df.loc[ndvi.index, 'ndvi'] = ndvi
        df.loc[bsi.index, 'bsi'] = bsi

        # Indicates trusted NDVI. NDVI which has been interpolated for a too long period is not trusted anymore.
        df['trusted_ndvi'] = df['real_ndvi'].rolling(12, min_periods=2, center=True).apply(lambda x: x.any())
        real_ndvi_dates = df.loc[df['real_ndvi']].index
        for d in range(len(real_ndvi_dates[:-1])):
            start_trusted = real_ndvi_dates[d]
            end_trusted = real_ndvi_dates[d+1]
            if (end_trusted - start_trusted).days < 42:
                df.loc[start_trusted:end_trusted, 'trusted_ndvi'] = True
        # Remove trust when NDVI and BSI are no longer related
        if anomaly_detector:
            index_an = df.loc[df['bsi'].notna()].index
            X_an = df.loc[index_an, ['bsi', 'ndvi']].values
            y_an = anomaly_detector.predict(X_an)
            df['anomaly'] = False
            df.loc[index_an, 'anomaly'] = (y_an == -1)
            df.loc[df['anomaly'], 'trusted_ndvi'] = False

        df['d_ndvi'] = df.ndvi.diff().round(4)

    # Interpolate coherence for every date.
    df['coh'] = df['coh'].interpolate()
    df['d_coh'] = np.abs(df['coh'].diff()).round(4)

    # Min, max and delta information for a period of 36D. These data feeds AI.
    win = df['coh'].rolling(36, min_periods=2, center=True)
    df['coh_min'] = win.min()
    df['coh_max'] = win.max()
    df['coh_delta'] = win.max() - win.min()

    # Season of every row (AI purpose)
    df['autumn'] = False
    df['winter'] = False
    df['spring'] = False
    df['summer'] = False
    df.loc[np.isin(df.index.month, [9, 10, 11]), 'autumn'] = True
    df.loc[np.isin(df.index.month, [12, 1, 2]), 'winter'] = True
    df.loc[np.isin(df.index.month, [3, 4, 5]), 'spring'] = True
    df.loc[np.isin(df.index.month, [6, 7, 8]), 'summer'] = True

    return df
