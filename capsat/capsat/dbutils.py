def load_spatialite(dbapi_conn, connection_record):
    """
    Loads spatialite extension.
    Has to be listened by the connect event from sqlalchemy.

    :param dbapi_conn:
    :param connection_record:
    :return:
    """
    dbapi_conn.enable_load_extension(True)
    try:
        dbapi_conn.load_extension('mod_spatialite')
    except:
        # If mod_spatialite doesn't work, try with spatialite.dll (windows)
        dbapi_conn.load_extension('spatialite')


class WeightedAverageAggregate:
    """
    Class implementing weighted average for SQLite
    """

    def __init__(self):
        self._sum = self._w_sum = 0

    def step(self, value, weight):
        if value is None or not weight:
            return
        self._w_sum += weight
        self._sum += weight * value

    def finalize(self):
        if self._w_sum == 0:
            return float('NaN')
        return self._sum / self._w_sum


class WeightedVarianceAggregate:
    """
    Class implementing weighted variance aggregate for SQLite
    For better understanding of variance computation, see https://fr.qaz.wiki/wiki/Algorithms_for_calculating_variance
    """

    def __init__(self):
        self._S = 0
        self._w_sum = 0
        self._mean = 0

    def step(self, value, weight):
        if value is None or not weight:
            return
        self._w_sum += weight
        mean_old = self._mean
        self._mean = mean_old + (weight / self._w_sum) * (value - mean_old)
        self._S += weight * (value - mean_old) * (value - self._mean)

    def finalize(self):
        """
        Return population variance

        :return:
        """
        if self._w_sum == 0:
            return float('NaN')
        return self._S / self._w_sum


class VarianceAggregate:
    """
    Class implementing variance aggregate for SQLite
    For better understanding of variance computation, see https://fr.qaz.wiki/wiki/Algorithms_for_calculating_variance
    """

    def __init__(self):
        self._S = 0
        self._N = 0
        self._mean = 0

    def step(self, value):
        if not value:
            return
        self._N += 1
        mean_old = self._mean
        self._mean = mean_old + (value - mean_old) / self._N
        self._S += (value - mean_old) * (value - self._mean)

    def finalize(self):
        """
        Return population variance

        :return:
        """
        if self._N == 0:
            return float('NaN')
        return self._S / self._N


def load_stat_aggregates(dbapi_conn, connection_record):
    """
    Creates aggregates for statistic purpose in SQLite.
    wavg = weighted average
    var = variance
    wvar = weighted variance

    :param dbapi_conn:
    :param connection_record:
    :return:
    """
    dbapi_conn.create_aggregate("wavg", 2, WeightedAverageAggregate)
    dbapi_conn.create_aggregate("var", 1, VarianceAggregate)
    dbapi_conn.create_aggregate("wvar", 2, WeightedVarianceAggregate)
