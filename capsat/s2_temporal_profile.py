#!/usr/bin/env python
# coding: utf-8

import os
import errno
import sys
import traceback

import argparse
import yaml

import logging.config
import logging.handlers

from datetime import datetime

from tqdm import tqdm

import multiprocessing as mp

from shapely.wkt import loads as shape_wkt

from sqlalchemy import create_engine, MetaData, Table, Column, String, Date, Float, text
from sqlalchemy.event import listen

from capsat import S2DataProvider, S2TemporalProfileBuilder
from capsat.dbutils import load_spatialite
from capsat.log import init_log_process, format_duration

_profile_builder = None
_feature = None
_uid_field = None
_worker_logger = None


def init_log_by_worker(queue: mp.Queue):
    """
    Init logging inside a process (worker) to a QueueHandler.
    Log records will be registered to queue.

    :param queue: Queue to register records
    :return:
    """
    queue_handler = logging.handlers.QueueHandler(queue)

    current_worker_name = mp.current_process().name

    # _worker_logger is global for each process (each process has its own).
    global _worker_logger
    _worker_logger = logging.getLogger(current_worker_name)

    # All existing handlers are removed in order to avoid logging to root logger.
    for h in _worker_logger.handlers:
        _worker_logger.removeHandler(h)
    _worker_logger.addHandler(queue_handler)
    _worker_logger.setLevel(logging.DEBUG)


def worker_count():
    """
    Defines how many worker will be used (1/4 of total CPU count). Max = 8.

    :return: Number of worker.
    """
    count = int(mp.cpu_count()/4)
    if count > 8:
        count = 8
    elif count < 1:
        count = 1
    return count


def init_worker(s2_provider, in_uid_field, in_feature, queue):
    """
    Initialize a worker inside pool.

    :param s2_provider: S2DataProvider to read S2 raster data
    :param in_feature: type of computed feature (NDVI or BSI)
    :param in_uid_field: ID field in LPIS data
    :param queue: Queue to register log records
    :return:
    """
    # Feature (NDVI or BSI)
    global _feature
    _feature = in_feature
    # Unique id field
    global _uid_field
    _uid_field = in_uid_field
    # Create object TemporalProfileCreator
    global _profile_builder
    _profile_builder = S2TemporalProfileBuilder(s2_provider)

    # Logging
    init_log_by_worker(queue)

    current_worker = mp.current_process()
    message = f"Start worker with PID {current_worker.pid}"
    _worker_logger.info(message)


def do_worker_unpack(i, in_feat):
    """
    Get temporal profile for one feature in LPIS data (and inside a worker)

    :param i:
    :param in_feat:
    :return:
    """
    feat_id = in_feat[_uid_field]
    geom = shape_wkt(in_feat['geometry'])
    pt_inside = geom.representative_point()
    try:
        profile = _profile_builder.get_temporal_profile(geom, _feature)
        _worker_logger.debug(f"Id {feat_id}")
        return feat_id, profile

    except Exception as e:
        _worker_logger.exception(f"Error with id {feat_id} ({int(pt_inside.x)}, {int(pt_inside.y)}): {e}")
        return feat_id, None


def do_worker(args):
    """
    Call do_worker_unpack with unpacked args

    :param args:
    :return:
    """
    return do_worker_unpack(*args)


def compute_temporal_profile(s2_provider, conn, lpis_table, lpis_id, in_feature, log_queue):
    """
    Compute temporal profile, using a multiprocessing.Pool

    :param s2_provider: S2DataProvider to read S2 raster data
    :param conn: SQLAlchemy connection to LPIS spatialite database
    :param lpis_table: LPIS table name
    :param lpis_id: LPIS id field
    :param in_feature: Type of feature to compute (NDVI or BSI)
    :param log_queue: Queue to record log
    :return:
    """
    try:
        # Create profile table
        metadata = MetaData(bind=conn)
        s2_profile = Table(
            's2_profile',
            metadata,
            Column("type", String),
            Column("lpis_id", String),
            Column("date", Date),
            Column("mean", Float),
            Column("median", Float),
            Column("var", Float),
            Column("masked_per", Float)
        )
        s2_profile.create(checkfirst=True)

        # Count total features to process
        sql_count = text(f"SELECT count(*) AS count FROM {lpis_table}")
        total_to_process = conn.execute(sql_count).fetchone()['count']

        # Create pool.
        total_processed = 0
        num_worker = worker_count()
        _worker_logger.info(f"Number of workers used: {num_worker}")
        p = mp.Pool(num_worker,
                    initializer=init_worker,
                    initargs=(s2_provider, lpis_id, in_feature, log_queue,))

        sql_select = text(f"SELECT {lpis_id} AS id, st_astext(geometry) AS geometry FROM {lpis_table}")
        proxy = conn.execution_options(stream_results=True).execute(sql_select)
        with tqdm(total=total_to_process) as pbar:
            while True:
                batch = proxy.fetchmany(5000)
                if not batch:
                    break
                to_insert = list()

                # Execute do_worker for each feature in batch.
                for feat_id, profile in p.imap_unordered(do_worker, enumerate(batch), chunksize=100):
                    if profile is None:
                        continue
                    profile_csv = [dict({"lpis_id": feat_id}, **item) for item in profile]
                    to_insert.extend(profile_csv)
                    total_processed += 1
                    _worker_logger.debug(f"Id {feat_id} written [{total_processed}/{total_to_process}]")
                    pbar.update()

                # Insert batch of profiles.
                conn.execute(s2_profile.insert(), to_insert)

        p.close()
        p.join()
        _worker_logger.info(f"Number of features processed: {total_processed}/{total_to_process}")

    except KeyboardInterrupt:
        print("Exit")

    except Exception as e:
        _worker_logger.exception(e)
        p.terminate()
        p.join()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Compute temporal profiles from sentinel 2 images")

    # Parameters file
    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    # Features to compute (NDVI, BSI)
    parser.add_argument('-f', '--feature', dest="feature", help="Feature to compute (NDVI, BSI).", required=True)

    # Log configuration
    parser.add_argument("--log", dest="log_conf", help="Log file configuration",
                        required=False, default="etc/log.yml")

    args = parser.parse_args()

    # Logging process
    if not os.path.exists(args.log_conf):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.log_conf)
    log_queue = mp.Queue(-1)
    log_process = mp.Process(target=init_log_process, args=(log_queue, args.log_conf))
    log_process.start()

    # Logging inside main process
    init_log_by_worker(log_queue)

    try:
        # Reading parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']
        s2_data_dir = params['parameters']['s2_data_dir']

        # Does database exist ?
        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)

        message = f"Start main process with PID {mp.current_process().pid}"
        _worker_logger.info(message)
        start = datetime.now()

        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, 'connect', load_spatialite)

        with S2DataProvider(s2_data_dir) as s2_data_provider, engine.connect() as conn:
            compute_temporal_profile(s2_data_provider, conn, lpis_table, lpis_id, args.feature, log_queue)

    except Exception as e:
        _worker_logger.exception(e)
        traceback.print_exc(file=sys.stderr)

    finally:
        end = datetime.now()
        _worker_logger.info(f"End (duration: {format_duration(start, end)}")

        # Stop logging
        log_queue.put(None)
        log_process.join()
