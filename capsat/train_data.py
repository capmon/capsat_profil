import os
import errno
import sys
import traceback

import argparse
import yaml

from tqdm import tqdm

from sqlalchemy import create_engine, text, MetaData, Table, Column, Float, Boolean, String, Integer
from sqlalchemy.event import listen

import numpy as np
import pandas as pd

from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.covariance import EllipticEnvelope
from sklearn.pipeline import Pipeline

from joblib import dump

from capsat.dbutils import load_spatialite, load_stat_aggregates
from capsat.profile import BSILogTransformer


_seasons = ['autumn', 'winter', 'spring', 'summer']


def train_anomaly_detector(conn, limit=100000):
    """
    Train a AI anomaly detection algorithm to isolate abnormal NDVI values.
    NDVI and BSI are linearly correlated, but only on "normal" weather.
    When there is snow, NDVI values cannot be trusted, so we eliminate theses values on dataset.

    :param conn: SQLAlchemy connection or engine
    :param limit: Number of values to consider for anomaly detection (too much values make detection harder)
    :return: Trained anomaly detector
    """
    sql = text(f"""
        SELECT bsi, ndvi
        FROM train_data
        WHERE dataset = 'train'
        ORDER BY random()
        LIMIT :limit
    """)
    df = pd.read_sql(sql, conn, params={'limit': limit})

    # This makes BSI distribution more symetric, even if it is not normally distributed
    pipe = Pipeline([
        ('bsi_log_transformer', BSILogTransformer()),
        ('elliptic_env', EllipticEnvelope())
    ])
    pipe.set_params(elliptic_env__contamination=0.015)

    # Train AI model
    X = df[['bsi', 'ndvi']].values
    pipe.fit(X)

    return pipe


def get_train_test_data(conn, code, ndvi_cover_threshold, ndvi_bare_threshold, anomaly_detector):
    """
    Get data from database for a given code and split them into train data and test data.

    Ground truth is built with this principle: if NDVI is beyond threshold, then we consider coherence value
    as a value where ground is covered. Below threshold, it is considered bare.
    This is obviously a twisted way to build ground truth, and it has inevitably a bias. But it works better with
    this AI training than with a traditional algorithm.

    :param conn: SQLAlchemy connection
    :param code: Crop code to train
    :param ndvi_cover_threshold: Threshold beyond which it is covered
    :param ndvi_bare_threshold: Threshold below which is bare
    :param anomaly_detector: Anomaly detector trained to remove abnormal NDVI values (snow, ...)
    :return: Tuple (X_train, X_test, y_train, y_test)
    """

    # Columns used for training.
    # Notice that NDVI is absent: NDVI is only used to build fake ground truth but cannot be involved in training,
    # because the purpose of using coherence is to overcome the lack of NDVI on certain dates.
    columns = ['coh', 'd_coh', 'coh_min', 'coh_max', 'coh_delta']
    columns.extend(_seasons)

    # Given the linear relation between NDVI and BSI, incoherent NDVI values are excluded from training.
    # Linearity is no longer true when there is snow and NDVI values are extremely low, even if soil is covered.
    sql = text(f"""
        SELECT * 
        FROM train_data 
        WHERE code = :code
    """)
    data = pd.read_sql(sql, conn, params={'code': code})

    # Anomaly detection and removal
    X_an = data[['bsi', 'ndvi']].values
    y_an = anomaly_detector.predict(X_an)
    data['anomaly'] = (y_an == -1)
    data.drop(data.loc[data['anomaly']].index, inplace=True)

    # This makes the difference between AI and traditional algorithm.
    data['autumn'] = False
    data['winter'] = False
    data['spring'] = False
    data['summer'] = False
    data.loc[data['month'].isin([9,10,11]), 'autumn'] = True
    data.loc[data['month'].isin([12,1,2]), 'winter'] = True
    data.loc[data['month'].isin([3,4,5]), 'spring'] = True
    data.loc[data['month'].isin([6,7,8]), 'summer'] = True

    # Build truth based on NDVI values.
    data['covered'] = np.nan
    data.loc[data['ndvi'] <= ndvi_bare_threshold, 'covered'] = 0
    data.loc[data['ndvi'] >= ndvi_cover_threshold, 'covered'] = 1
    data.drop(data.loc[data['covered'].isna()].index, inplace=True)

    # Split train and test data
    train_data = data.loc[data['dataset'] == 'train']
    test_data = data.loc[data['dataset'] == 'test']
    X_train = train_data[columns].values
    X_test = test_data[columns].values
    y_train = train_data['covered']
    y_test = test_data['covered']

    return X_train, X_test, y_train, y_test


if '__main__' == __name__:

    parser = argparse.ArgumentParser(description="Train AI to recognize coverage from coherence data.")

    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    args = parser.parse_args()

    try:
        # Load parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']
        models_path = params['parameters']['models_path']
        ndvi_cover_threshold = params['parameters']['ndvi_cover_threshold']
        ndvi_bare_threshold = params['parameters']['ndvi_bare_threshold']

        # Does models_path exist ?
        if not os.path.exists(models_path) or not os.path.isdir(models_path):
            os.makedirs(models_path)

        # Connect database
        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)
        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, "connect", load_spatialite)
        listen(engine, "connect", load_stat_aggregates)

        # Create train_result table
        with engine.connect() as conn:
            metadata = MetaData(bind=conn)
            train_result = Table(
                'train_result',
                metadata,
                Column('code', String),
                Column('train_size', Integer),
                Column('method', String),
                Column('accuracy', Float),
                Column('precision', Float),
                Column('recall', Float),
                Column('f_score', Float)
            )
            train_result.drop(checkfirst=True)
            train_result.create()

            # Train anomaly detector
            anomaly_detector = train_anomaly_detector(conn)
            dump(anomaly_detector, os.path.join(models_path, "anomaly_detector.joblib"))

            code_rows = conn.execute(text("SELECT code FROM crop_code"))
            num_codes = conn.execute(text("SELECT count(*) AS count FROM crop_code")).fetchone()['count']

            with tqdm(total=num_codes) as pbar:
                for row in code_rows:
                    code = row['code']
                    X_train, X_test, y_train, y_test = get_train_test_data(conn, code,
                                                                           ndvi_cover_threshold, ndvi_bare_threshold,
                                                                           anomaly_detector)
                    m_train = X_train.shape[0]
                    m_test = X_test.shape[0]

                    # Train model
                    if m_train < 50000:
                        pipe = Pipeline([
                            ('scaler', StandardScaler()),
                            ('model', SVC())
                        ])
                        pipe.set_params(model__kernel='rbf', model__C=100)
                        method = "SVM"
                    else:
                        pipe = Pipeline([
                            ('feature', PolynomialFeatures()),
                            ('scaler', StandardScaler()),
                            ('model', SGDClassifier())
                        ])
                        pipe.set_params(feature__degree=5)
                        method = "SGD"
                    pipe.fit(X_train, y_train)

                    # Evaluate model
                    y_predict = pipe.predict(X_test)

                    # Store results
                    metrics = {
                        'code': code,
                        'train_size': m_train,
                        'method': method,
                        'accuracy': accuracy_score(y_test, y_predict).round(3),
                        'precision': precision_score(y_test, y_predict).round(3),
                        'recall': recall_score(y_test, y_predict).round(3),
                        'f_score': f1_score(y_test, y_predict).round(3)
                    }
                    conn.execute(train_result.insert(), metrics)

                    # Persist models
                    dump(pipe, os.path.join(models_path, f"{code}_pipe.joblib"))

                    pbar.update()

    except Exception as e:
        traceback.print_exc(file=sys.stderr)