import os
import sys
import traceback
import errno
from tqdm import tqdm

import argparse

import yaml
import logging.config

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Date, Float, Boolean, text
from sqlalchemy.event import listen
from geoalchemy2 import Geometry

from shapely.wkt import loads as shape_wkt

from capsat import S1DataProvider
from capsat import S1TemporalProfileBuilder


def load_spatialite(dbapi_conn, connection_record):
    """
    Loads spatialite extension.

    :param dbapi_conn:
    :param connection_record:
    :return:
    """
    dbapi_conn.enable_load_extension(True)
    dbapi_conn.execute('SELECT load_extension("mod_spatialite.so")')


def compute_s1_edges(conn, provider: S1DataProvider):
    """
    Vectorizes edge of data for each S1 raster in provider and inserts them in spatialite database.

    :param conn: SQLAlchemy connection.
    :param provider: S1DataProvider
    :return:
    """
    # Get data edges from S1 coherence tiles (ie: vectorize area with data)
    features = list()
    crs = None
    with tqdm(total=provider.size()) as pbar:
        for tile in provider:
            if not crs:
                crs = tile.crs
            features.append({
                "orbit": tile.orbit,
                "polarization": tile.polarization,
                "start_date": tile.start_date.date(),
                "end_date": tile.end_date.date(),
                "geometry": f"SRID={crs.to_epsg()};{tile.get_data_geometry(20).wkt}"
            })
            pbar.update()

    # Create tables
    metadata = MetaData(bind=engine)
    s1_edge = Table(
        's1_coh_edge',
        metadata,
        Column('orbit', Integer),
        Column('polarization', String),
        Column('start_date', Date),
        Column('end_date', Date),
        Column('geometry', Geometry(geometry_type='GEOMETRY', srid=crs.to_epsg(), management=True))
    )
    s1_edge.create()
    conn.execute(s1_edge.insert(), features)


def compute_s1_occurrence(conn, lpis_table, lpis_id, lpis_srid):
    """
    Counts occurrences where lpis geometry is within S1 edges.

    :param conn: SQLAlchemy connection
    :param lpis_table: LPIS table name
    :param lpis_id: LPIS id field
    :param lpis_srid: SRID of LPIS geometry column
    :return:
    """
    # Create occurrence table
    metadata = MetaData(bind=conn)
    s1_occurrence = Table(
        's1_coh_occurrence',
        metadata,
        Column("lpis_id", String),
        Column("orbit", Integer),
        Column("polarization", String),
        Column("occurrence", Integer)
    )
    s1_occurrence.create()

    # Insert into occurrence table
    total = conn.execute(text(f"SELECT count(*) AS count FROM {lpis_table}")).fetchone()['count']
    sql_select = text(f"SELECT {lpis_id} AS lpis_id FROM {lpis_table}")
    proxy = conn.execution_options(stream_results=True).execute(sql_select)
    sql = text(f"""
            SELECT r.{lpis_id} AS lpis_id, e.orbit, e.polarization, count(*) AS occurrence
            FROM s1_coh_edge e
            JOIN {lpis_table} r 
                ON st_contains(e.geometry, st_transform(setsrid(r.geometry, {lpis_srid}), srid(e.geometry))) = 1
            WHERE r.{lpis_id} = :id
            GROUP BY orbit, polarization
        """)
    with tqdm(total=total) as pbar:
        while True:
            batch = proxy.fetchmany(1000)
            if not batch:
                break
            to_insert = list()
            for feat in batch:
                rows = conn.execute(sql, id=feat['lpis_id'])
                to_insert.extend(rows.fetchall())
                pbar.update()
            conn.execute(s1_occurrence.insert(), to_insert)


def compute_temporal_profile(s1_provider: S1DataProvider, conn, lpis_table, lpis_id, lpis_srid):
    """
    Compute temporal profile from Sentinel 1 data.

    :param s1_provider: S1DataProvider to read S1 raster
    :param conn: SQLAlchemy connection to LPIS database
    :param lpis_table: LPIS table name
    :param lpis_id: LPIS id field
    :param lpis_srid: SRID of LPIS geometry column
    :return:
    """
    try:
        logger = logging.getLogger('app')

        # Create table containing results
        metadata = MetaData(bind=conn)
        s1_coh_profile = Table(
            's1_coh_profile',
            metadata,
            Column('type', String),
            Column('lpis_id', String),
            Column('start_date', Date),
            Column('end_date', Date),
            Column('orbit', Integer),
            Column('ascending', Boolean),
            Column('polarization', String),
            Column('mean', Float),
            Column('median', Float),
            Column('var', Float)
        )
        s1_coh_profile.create()

        profile_builder = S1TemporalProfileBuilder(s1_provider, lpis_srid)

        sql_count = text(f"SELECT count(*) AS count FROM {lpis_table}")
        total = conn.execute(sql_count).fetchone()['count']

        sql_select = text(f"SELECT {lpis_id} AS id, st_astext(geometry) AS geometry FROM {lpis_table}")
        proxy = conn.execution_options(stream_results=True).execute(sql_select)
        with tqdm(total=total) as pbar:
            while True:
                batch = proxy.fetchmany(5000)
                if not batch:
                    break
                to_insert = list()
                for feat in batch:
                    try:
                        feat_id = feat['id']
                        geom = shape_wkt(feat['geometry'])

                        # Find orbit and polarization to browse S1 raster.
                        sql = text("""
                            SELECT o.orbit, o.polarization, o.occurrence
                            FROM s1_coh_occurrence o
                            WHERE o.lpis_id = :id
                            ORDER BY o.occurrence DESC
                            LIMIT 1
                        """)
                        result = conn.execute(sql, id=feat_id).fetchone()
                        orbit = result['orbit']
                        polarization = result['polarization']

                        # Compute profile
                        profile = profile_builder.get_temporal_profile(geom, "coherence", orbit, polarization)
                        profile_csv = [dict({"lpis_id": feat_id}, **item) for item in profile]
                        to_insert.extend(profile_csv)

                    except Exception as e:
                        logger.exception(e)
                        continue

                    finally:
                        pbar.update()

                # Insert batch of profiles
                conn.execute(s1_coh_profile.insert(), to_insert)

    except KeyboardInterrupt:
        print("exit")

    except Exception as e:
        logger.exception(e)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Compute temporal profile from Sentinel 1 rasters.")

    # Parameters file
    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    # Skip precalculus
    parser.add_argument("-s", "--skip-precalculus", dest="skip_precalculus", help="Skip first steps of precalculus",
                        required=False, default=False, action="store_true")

    # Log level
    parser.add_argument("--log", dest="log_conf", help="Log file configuration",
                        required=False, default="etc/log.yml")

    args = parser.parse_args()

    try:
        # Logging
        if not os.path.exists(args.log_conf):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.log_conf)
        with open(args.log_conf, 'r') as f:
            log_conf = yaml.safe_load(f)
            logging.config.dictConfig(log_conf)

        # Reading parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']
        lpis_srid = params['parameters']['lpis_srid']
        s1_data_dir = params['parameters']['s1_data_dir']

        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)

        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, 'connect', load_spatialite)

        with S1DataProvider(s1_data_dir) as provider, engine.connect() as conn:
            if not args.skip_precalculus:
                compute_s1_edges(conn, provider)
                compute_s1_occurrence(conn, lpis_table, lpis_id, lpis_srid)
            compute_temporal_profile(provider, conn, lpis_table, lpis_id, lpis_srid)

    except Exception as e:
        traceback.print_exc(file=sys.stderr)

