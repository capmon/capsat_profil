import os
import errno
import sys, traceback
import shutil

from tqdm import tqdm

import argparse
import yaml

from sqlalchemy import create_engine, text, MetaData, Table, Column, Integer, String
from sqlalchemy.event import listen

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from capsat.dbutils import load_spatialite, load_stat_aggregates


def graph_general(engine, experiment_start, experiment_end, graph_path):
    """
    Draw general graph, ie average covered and bare days for all experiment duration and all codes.

    :param engine: SQLAlchemy engine
    :param experiment_start: First day of experiment
    :param experiment_end: Last day of experiment
    :param graph_path: Output graph path
    :return:
    """
    sql = text(f"""
                SELECT 
                    lpis_id,
                    code,
                    covered_tr1 + covered_tr2 + covered_tr3 + covered_tr4 AS covered,
                    bare_tr1 + bare_tr2 + bare_tr3 + bare_tr4 AS bare
                FROM coverage
            """)
    df = pd.read_sql(sql, engine, index_col="lpis_id")

    # Group by code and compute mean, std and median values
    dfg = df.groupby("code")
    mean = dfg.mean()
    mean.sort_values('covered', ascending=False, inplace=True)
    std = dfg.std()
    std.rename(columns={'covered': 'std'}, inplace=True)
    mean = mean.join(std['std'])
    median = dfg.median()

    # Draw graph
    fig, ax = plt.subplots(1, figsize=(20, 15))
    ax.barh(mean.index, mean['covered'], xerr=mean['std'], label="Sol couvert", alpha=0.7)
    ax.barh(mean.index, mean['bare'], left=mean['covered'], label="Sol nu", alpha=0.7)
    ax.scatter(median['covered'], median.index, color='yellow', label="Médiane sol couvert", zorder=10)
    ax.legend()
    ax.grid(axis="x")
    ax.set_xlabel("Cumul de jours")
    ax.set_ylabel("Code culture")
    ax.set_title(
        f"Moyennes sol couvert/sol nu entre le {experiment_start.strftime('%d-%m-%Y')} et le {experiment_end.strftime('%d-%m-%Y')}")
    fig.savefig(graph_path)


def graph_by_code_and_quarter(engine, code, graph_path):
    """
    Draw coverage graph by crop code and quarter.

    :param engine: SQLAlchemy engine or connection
    :param code: Crop code to process
    :param graph_path: Output graph path
    :return:
    """
    # Get coverage data
    sql = text(f"""
        SELECT lpis_id, covered_tr1 AS tr1, covered_tr2 AS tr2, covered_tr3 AS tr3, covered_tr4 AS tr4
        FROM coverage
        WHERE code = :code
    """)
    covered = pd.read_sql(sql, engine, params={'code': code}, index_col="lpis_id")

    # Get bare data
    sql = text(f"""
        SELECT lpis_id, bare_tr1 AS tr1, bare_tr2 AS tr2, bare_tr3 AS tr3, bare_tr4 AS tr4
        FROM coverage
        WHERE code = :code
    """)
    bare = pd.read_sql(sql, engine, params={'code': code}, index_col="lpis_id")

    # Mix them and compute mean, std and median
    mean = pd.concat([covered.mean(), bare.mean()], axis=1)
    std = pd.concat([covered.std(), bare.std()], axis=1)
    median = pd.concat([covered.median(), bare.median()], axis=1)

    # Draw graph
    fig, ax = plt.subplots(figsize=(10, 8))
    ax.bar(mean.index, mean[0], yerr=std[0], label="Moyenne sol couvert", alpha=0.7)
    ax.bar(mean.index, mean[1], bottom=mean[0], label="Moyenne sol nu", alpha=0.7)
    ax.scatter(median.index, median[0], color="yellow", label="Médiane sol couvert", zorder=10)
    ax.grid(axis="y")
    ax.legend()
    ax.set_title(f"Moyenne jours couverts et nus par trimestre pour le code {code}")
    fig.savefig(graph_path)


def graph_probability(engine, code, graph_path):
    """
    Draw graph of coverage probability for one crop code.

    :param engine: SQLAlchemy engine or connection
    :param code: Crop code
    :param graph_path: Output graph path
    :return:
    """
    sql = text(f"""
        SELECT * FROM coverage_probability
    """)
    proba = pd.read_sql(sql, engine, parse_dates=['date'], index_col='date')

    fig, ax = plt.subplots(figsize=(15, 8))
    ax.plot(proba[code])
    ax.set_ylabel("Probabilité")
    ax.set_title(f"Probabilité de couverture pour le code {code}")
    ax.grid()
    fig.savefig(graph_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Draws statistics graphs")

    parser.add_argument("-p", "--params", dest="params", help="Parameters file",
                        required=False, default="etc/parameters.yml")

    # Log level
    parser.add_argument("--log", dest="log_conf", help="Log file configuration",
                        required=False, default="etc/log.yml")

    args = parser.parse_args()

    try:
        # Load parameters
        if not os.path.exists(args.params):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.params)
        with open(args.params, 'r') as f:
            params = yaml.safe_load(f)
        lpis_database = params['parameters']['lpis_database']
        lpis_table = params['parameters']['lpis_table']
        lpis_id = params['parameters']['lpis_id']
        experiment_start = pd.to_datetime(params['parameters']['experiment_start'], format="%Y-%m-%d").date()
        experiment_end = pd.to_datetime(params['parameters']['experiment_end'], format="%Y-%m-%d").date()

        # Connect database
        if not os.path.exists(lpis_database):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), lpis_database)
        engine = create_engine(f"sqlite:///{lpis_database}")
        listen(engine, "connect", load_spatialite)
        listen(engine, "connect", load_stat_aggregates)

        # Output path
        directory = os.path.split(lpis_database)[0]
        graph_directory = os.path.join(directory, "output_graphs")
        if os.path.exists(graph_directory):
            shutil.rmtree(graph_directory)
        os.makedirs(graph_directory)

        # General graph (all crop codes, entire year)
        graph_path = os.path.join(graph_directory, "all_codes.jpg")
        graph_general(engine, experiment_start, experiment_end, graph_path)

        # By code and quarter
        with engine.connect() as conn:
            codes = conn.execute(text(f"SELECT code FROM crop_code")).fetchall()
            for code in tqdm(codes):
                code = code[0]

                # Graph by code and quarter
                graph_path = os.path.join(graph_directory, f"coverage_{code}.jpg")
                graph_by_code_and_quarter(engine, code, graph_path)

                # Probability of coverage
                graph_path = os.path.join(graph_directory, f"coverage_probability_{code}.jpg")
                graph_probability(engine, code, graph_path)

    except:
        traceback.print_exc(file=sys.stderr)