# capsat_profile

Toolbox for computing and analysing Sentinel 1 & 2 temporal profiles as part of CAP monitoring.

## License 
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.html) for code

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/) for data (data/test folders)

## Python environment

This project has been with conda package & environment management system.

You first need to download either [Anaconda](https://www.anaconda.com/products/individual) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html) and install it.

Then, create an environment dedicated to capsat_profile, activate it, and install dependencies

```
conda create --name capsat
conda activate capsat
conda install -c conda-forge rasterio pyyaml tqdm shapely sqlalchemy geoalchemy2 numpy pyproj pandas geopandas matplotlib scipy jupyter notebook pip
pip install utm
```

## Parameters

All project parameters must be store in a file `parameters.yml`. By default, this file is stored in `etc` directory, but it can be changed by using `-p` option of each script.
An example is supplied in `etc/parameters.yml.sample`.

## Log configuration

The project needs a configuration file for logging. The default path for this file is `etc/log.yml`, but you can specify it with `--log` option. There already is a `log.yml.sample` in `etc` directory, you can make a copy and customize it.

## Tools

### Sentinel 2 temporal profiles (NDVI and BSI indexes)

This is done by `s2_temporal_profile.py`. Use it to create NDVI and BSI profiles from Sentinel 2 data.

```
usage: s2_temporal_profile.py [-h] [-p PARAMS] -f FEATURE [--log LOG_CONF]

Compute temporal profiles from sentinel 2 images

optional arguments:
  -h, --help                        show this help message and exit
  -p PARAMS, --params PARAMS        Parameters file
  -f FEATURE, --feature FEATURE     Feature to compute (NDVI, BSI).
  --log LOG_CONF                    Log file configuration

```


### Sentinel 1 coherence temporal profile

This is done by `s1_temporal_profile.py`. Use it to create temporal profile for Sentinel 1 radar coherence.

```
usage: s1_temporal_profile.py [-h] [-p PARAMS] [-s] [--log LOG_CONF]

Compute temporal profile from Sentinel 1 rasters.

optional arguments:
  -h, --help                      show this help message and exit
  -p PARAMS, --params PARAMS      Parameters file
  -s, --skip-precalculus
                                  Skip first steps of precalculus
  --log LOG_CONF                  Log file configuration
```

The option `-s` is designed to skip precalculus, which vectorizes Sentinel 1 raster edges and stores which raster is available for a given orbit and polarization.
Even if this step can be skipped, it must at least be runned once.


### Merging Sentinel 1 & 2 profile data

NDVI & BSI indexes are store in table `s2_profile`. Coherence index is stored in `s1_coh_profile`.
In these tables, one row corresponds to one index for one LPIS parcel and one date.
`merge_s1_s2.py` aggregates NDVI, BSI and coherence in a single line for one parcel and one date, and store the result in table `join_profile`.
This operation avoids long and tricky JOIN queries in SQL, and makes data analysis more convenient.

```
usage: merge_s1_s2.py [-h] [-p PARAMS]

Merge S1 and S2 profile tables

optional arguments:
  -h, --help                    show this help message and exit
  -p PARAMS, --params PARAMS    Parameters file
```

### Preparing train data

In order to train AI to recognize coverage from coherence information, train data are built with `prepare_train_date.py`.
Coherence is considered covered when corresponding NDVI is beyond 0.4.
It is considered bare when NDVI is below 0.3.
Those a loose values, but the purpose of this script is only to store data in table `train_data`. When actually training, better values can be chosen by selecting in `train_data`.

```
usage: prepare_train_data.py [-h] [-p PARAMS]

Prepare and store data which can be used to train AI to recognize coverage from coherence

optional arguments:
  -h, --help                    show this help message and exit
  -p PARAMS, --params PARAMS    Parameters file

```

### Training AI to recognize coverage from coherence data

`train_data.py` trains one AI model for each crop code to recognize coverage from coherence values.

```
usage: train_data.py [-h] [-p PARAMS]

Train AI to recognize coverage from coherence data.

optional arguments:
  -h, --help                    show this help message and exit
  -p PARAMS, --params PARAMS    Parameters file

```

Notice that the evaluation of training are store in table `train_result`.

### Analysing profiles to get coverage and bare soil statistics

This finally counts how many days are considered bare soil or covered for each parcel. Results are stored in table `coverage`.

```
usage: profile_analysis.py [-h] [-p PARAMS] [--log LOG_CONF]

Temporal profile analysis

optional arguments:
  -h, --help                    show this help message and exit
  -p PARAMS, --params PARAMS    Parameters file
  --log LOG_CONF                Log file configuration

```

### Draw coverage graphs

Done with `coverage_graphs.py`. This script draws graph with statistics of covered and bare soil.

```
usage: coverage_graphs.py [-h] [-p PARAMS] [--log LOG_CONF]

Draws statistics graphs

optional arguments:
  -h, --help                    show this help message and exit
  -p PARAMS, --params PARAMS    Parameters file
  --log LOG_CONF                Log file configuration

```